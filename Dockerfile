FROM python:3.9-slim

ENV PYTHONUNBUFFERED 1

ENV APP_HOME /app
WORKDIR $APP_HOME

COPY ./requirements.txt /app/requirements.txt

RUN pip install --upgrade pip
RUN pip install -r /app/requirements.txt

COPY ./standings /app/standings

EXPOSE 5000

CMD python -m standings.main