build:
	docker-compose build

run:
	docker-compose up

test:
	docker-compose run --rm standings pytest

lint:
	docker-compose run --rm standings flake8